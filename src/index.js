const path = require('path');
const express = require('express');
const { getToken, getOAuthData } = require('./api/oauth');
const AmoApi = require('./api/amo');
const { v4: uuid4 } = require('uuid');

const PORT = 3000;

const app = express();

app.use(function (req, res, next) {
  console.log(req.path);
  next();
});

//#region Webpack HMR
var webpack = require('webpack');
var webpackConfig = require('../webpack.config');
var compiler = webpack(webpackConfig);

app.use(require("webpack-dev-middleware")(compiler, {
  publicPath: webpackConfig.output.publicPath
}));

app.use(require("webpack-hot-middleware")(compiler, {
  path: "/__webpack_hmr",
  timeout: 20000,
}));

//#endregion Webpack HMR

app.use(express.static(path.join(__dirname, '..', 'public')))
app.use(express.json());


function createHandlerFactory(type) {
  return function (req, res) {
    getOAuthData().then(({ base_domain, access_token }) => {
      let amo = new AmoApi(base_domain, access_token)
      return amo[`create${type}`](req.body)
    }).then(id => res.json(id).end())
      .catch(err => res.status(400).end(JSON.stringify(err)))
  }
}

app.post('/api/contacts', createHandlerFactory('Contact'))
app.post('/api/leads', createHandlerFactory('Lead'))
app.post('/api/companies', createHandlerFactory('Company'))

app.post('/graphql', function (req, res) {
  res.status(400).end('Not Implemented!');
})

app.listen(PORT, () => {
  console.log(`Server is running on http://localhost:${PORT}`)
});
