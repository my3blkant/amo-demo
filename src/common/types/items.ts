export const enum ItemType {
  LEAD = "lead",
  CONTACT = "contact",
  COMPANY = "company",
}

export class Item {
  id: string;
  type: ItemType;
  name: string;
}
export type ContactID = number;
export type CompanyID = number;
export type LeadID = number;

export type Contact = {
  id?: ContactID;
  name: string;
}

export type Company = {
  id?: CompanyID;
  name: string;
}

export type Lead = {
  id?: LeadID;
  name: string;
}
