import { ItemType } from "./types/items";


export const TYPES_NAMES: { [key: string]: string } = {
  [ItemType.LEAD]: "Сделка",
  [ItemType.CONTACT]: "Контакт",
  [ItemType.COMPANY]: "Компания",
};
