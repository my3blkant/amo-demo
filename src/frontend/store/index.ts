import Vuex from 'vuex'

import { Item } from '@common/types/items'


function createStore() {
  return new Vuex.Store({
    state: {
      creation_history: [],
    },
    mutations: {
      addHistoryRecord(state, record: Item) {
        state.creation_history = [record, ...state.creation_history]
      }
    }
  })
}
export default createStore;
