import Dropdown from './Dropdown.vue';
import { action, actions } from '@storybook/addon-actions';

export default {
  title: 'Amo/Dropdown',
  component: Dropdown,
}

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  template: '<Dropdown v-bind="$props" @change="onchange" :style="{ width: \'350px\' }"></Dropdown>',
  components: {
    Dropdown,
  },
  methods: actions("onchange"),
})
const defaultArgs = {
  unselectedText: 'Not Invented Here',
  options: [
    { id: 1, title: 'Create bicycle' },
    { id: 2, title: 'Use a crutch' },
    { id: 3, title: 'Rewrite from scratch' },
  ]
};


export const NotSelected = Template.bind({});
NotSelected.args = {
  ...defaultArgs,
};
export const OpenedNotSelected = Template.bind({});
OpenedNotSelected.args = {
  ...defaultArgs,
  initialOpened: true,
};

export const OpenedSelected = Template.bind({});
OpenedSelected.args = {
  ...defaultArgs,
  initialOpened: true,
  value: defaultArgs.options[0],
};

export const Selected = Template.bind({});
Selected.args = {
  ...defaultArgs,
  value: defaultArgs.options[1],
};

