import { action, actions } from '@storybook/addon-actions';
import ActionButton from './ActionButton.vue';

export default {
  title: 'Amo/ActionButton',
  component: ActionButton,
  argTypes: {

  }
}

const Template = (args, { argTypes }) => ({
  props: Object.keys(argTypes),
  components: { ActionButton },
  template: '<ActionButton v-bind="$props" @click="click"></ActionButton>',
  methods: actions('click'),
});
const templateArgs = {
  label: 'Action',

}

export const Active = Template.bind({});
Active.args = {
  ...templateArgs,
  active: true,
}

export const Disabled = Template.bind({});
Disabled.args = {
  ...templateArgs,
}

export const Loading = Template.bind({});
Loading.args = {
  ...templateArgs,
  active: true,
  loading: true,
}
