import { Company, CompanyID, Contact, ContactID, ItemType, Lead, LeadID } from "@common/types/items";
import axios from 'axios';

export function createItem(itemType: ItemType.COMPANY, data: Company): Promise<CompanyID>
export function createItem(itemType: ItemType.CONTACT, data: Contact): Promise<ContactID>
export function createItem(itemType: ItemType.LEAD, data: Lead): Promise<LeadID>
export function createItem(itemType: ItemType, data: any): Promise<number> {
  if (itemType in methods.create) {
    return methods.create[itemType](data);
  } else {
    return Promise.reject(new Error("Item type not exists"));
  }
}


/// TODO: make some auth?
const methods = {
  create: {
    [ItemType.COMPANY](company: Company): Promise<any> {
      return axios.post('/api/companies', company).then(res => res.data)
    },
    [ItemType.CONTACT](contact: Contact): Promise<any> {
      return axios.post('/api/contacts', contact).then(res => res.data)
    },
    [ItemType.LEAD](lead: Lead): Promise<any> {
      return axios.post('/api/leads', lead).then(res => res.data)
    }
  } as {
    [m: string]: (o: any) => Promise<any>
  }
}
