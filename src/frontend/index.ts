import Vue from 'vue/dist/vue.esm.js';
import App from './app.vue';
import Vuex from 'vuex';

import { library } from '@fortawesome/fontawesome-svg-core';
import { faSpinner, faCheck, faChevronDown } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

library.add(faSpinner);
library.add(faCheck);
library.add(faChevronDown);

Vue.component('fa-icon', FontAwesomeIcon)
Vue.use(Vuex)

import createStore from './store';

const store = createStore();

let app = new Vue({
  render: (h: any) => h(App),
  store,
})


app.$mount('#app-container');
