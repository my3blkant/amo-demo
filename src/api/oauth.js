const axios = require('axios').default;

const OAUTH_URL = process.env.OAUTH_URL
const OAUTH_CLIENT_ID_HEADER = process.env.OAUTH_CLIENT_ID_HEADER
const OAUTH_CLIENT_ID = process.env.OAUTH_CLIENT_ID


function getDefaultOAuthConfig() {
  return {
    headers: {
      [OAUTH_CLIENT_ID_HEADER]: OAUTH_CLIENT_ID
    }
  }
}

function getOAuthData() {
  return axios.get(OAUTH_URL, getDefaultOAuthConfig())
    .then(res => res.data)
}

function getToken() {
  return getOAuthData().then(json => json.access_token)
}

module.exports = {
  getToken,
  getOAuthData,
};
