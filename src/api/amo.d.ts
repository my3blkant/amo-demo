import { Company, Lead, Contact, CompanyID, ContactID, LeadID } from "@common/types/items";

declare class AmoAPI {
  constructor(base_domain: string, access_token: string);

  createContact(contact: Contact): Promise<ContactID>;
  createCompany(contact: Company): Promise<CompanyID>;
  createLead(contact: Lead): Promise<LeadID>;

}

export = AmoAPI;
