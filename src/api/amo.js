const axios = require('axios').default;

const CONTACTS_PATH = '/api/v4/contacts';
const COMPANIES_PATH = '/api/v4/companies';
const LEADS_PATH = '/api/v4/leads';

class AmoAPI {
  constructor(base_domain, access_token) {
    this.base_domain = base_domain;
    this.access_token = access_token;
  }

  _getHeaders() {
    return {
      Authorization: `Bearer ${this.access_token}`,
    }
  }

  _getBaseUrl() {
    return `https://${this.base_domain}/`;
  }

  _createRequest(path, method = 'get', data = null) {
    return {
      method,
      url: path,
      baseURL: this._getBaseUrl(),
      headers: this._getHeaders(),
      data,
    }
  }

  createContact(contact) {
    return axios(this._createRequest(CONTACTS_PATH, 'post', [contact]))
      .then(res => res.data._embedded.contacts[0].id)
  }

  createCompany(contact) {
    return axios(this._createRequest(COMPANIES_PATH, 'post', [contact]))
      .then(res => res.data._embedded.companies[0].id)
  }

  createLead(contact) {
    return axios(this._createRequest(LEADS_PATH, 'post', [contact]))
      .then(res => res.data._embedded.leads[0].id)
  }

}

module.exports = AmoAPI;
