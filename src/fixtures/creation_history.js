module.exports = [
  {
    "id": "1",
    "type": "lead",
    "name": "Lead #1"
  },
  {
    "id": "2",
    "type": "contact",
    "name": "Contact #1"
  },
  {
    "id": "3",
    "type": "company",
    "name": "Company #1"
  }
]
