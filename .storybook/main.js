const custom = require('../webpack.config.js');

module.exports = {
  "stories": [
    "../src/**/*.stories.mdx",
    "../src/**/*.stories.@(js|jsx|ts|tsx)"
  ],
  "addons": [
    "@storybook/addon-links",
    "@storybook/addon-essentials"
  ],
  webpackFinal(config) {

    config.resolve.extensions = [...config.resolve.extensions, ...custom.resolve.extensions]
    config.resolve.alias = { ...config.resolve.alias, ...custom.resolve.alias }
    return config;
    // return {
    //   ...config, module: {
    //     ...config.module,
    //     rules: [
    //       ...config.module.rules,
    //       ...custom.module.rules
    //     ]
    //   }
    // };
  }
}

// "vue-loader15": "npm:vue-loader@^15.0",
