import Vue from 'vue/dist/vue.esm.js';

import { library } from '@fortawesome/fontawesome-svg-core';
import { faSpinner, faCheck, faChevronDown } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

library.add(faSpinner);
library.add(faCheck);
library.add(faChevronDown);

Vue.component('fa-icon', FontAwesomeIcon)

export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
}
