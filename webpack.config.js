const path = require('path');
const VueLoaderPlugin = require('vue-loader/lib/plugin')
const webpack = require('webpack');
const mode = process.env.NODE_ENV || 'production';

module.exports = {
  mode,
  entry: [
    './src/frontend/index.ts',
    // TODO: only for dev mode
    "webpack-hot-middleware/client",
  ],
  "devtool": "inline-source-map",
  // TODO: only for dev mode
  devServer: {
    contentBase: './build/development/',
    hot: true,
  },
  output: {
    filename: 'bundle.js',
    path: path.join(__dirname, 'build', mode),
    publicPath: '/static',
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        loader: 'ts-loader',
        exclude: /node_modules/,
        options: {
          // For lang=ts in .vue
          appendTsSuffixTo: [/\.vue$/]
        },
      },
      {
        test: /\.vue$/,
        use: 'vue-loader',
        exclude: /node_modules/,
      },
      {
        test: /\.js$/,
        use: 'babel-loader'
      },
      {
        test: /\.css$/,
        use: [
          'vue-style-loader',
          'style-loader',
          'css-loader',
        ]
      },

    ],
  },
  plugins: [
    new VueLoaderPlugin(),
    new webpack.HotModuleReplacementPlugin(), // TODO: only for dev mode

  ],
  resolve: {
    extensions: ['.tsx', '.ts', '.js', '.vue'],
    alias: {
      "@": path.join(__dirname, "src", "frontend"),
      "@common": path.join(__dirname, "src", "common"),
      "@fixtures": path.join(__dirname, "src", "fixtures"),
    },
  },
}
